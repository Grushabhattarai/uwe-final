/* b.Write a program that reads the above test.bin them and copies the employees’ data whose
salary is more than an amount entered by the user in the data.bin binary file. The program 
should also display the average salary of the employees stored in data.bin */


#include <iostream>
#include <fstream>
using namespace std;

struct Employee
{
    string name;
    int tax_number;
    float salary;
};

int main()
{
    // Open test.bin file for reading in binary mode
    ifstream test_file("test.bin", ios::binary);

    // Open data.bin file for writing in binary mode
    ofstream data_file("data.bin", ios::binary);

    float average_salary = 0;
    int count = 0;
    float salary_threshold;

    // Prompt the user to enter the salary threshold
    cout << "Enter the salary threshold: ";
    cin >> salary_threshold;

    Employee employees;
    
    // Read data from test.bin file
    while (test_file.read((char *)&employees, sizeof(Employee)))
    {
        // Check if the salary is greater than the threshold
        if (employees.salary > salary_threshold)
        {
            // Write data to data.bin file
            data_file.write((char *)&employees, sizeof(Employee));

            // Accumulate salary for average calculation
            average_salary += employees.salary;

            // Count the number of employees
            count++;
        }
    }

    // Calculate the average salary
    average_salary /= count;

    // Display the average salary
    cout << "The average salary of the employees stored in data.bin is: " << average_salary << endl;

    // Close the test.bin file
    test_file.close();

    // Close the data.bin file
    data_file.close();

    return 0;
}
