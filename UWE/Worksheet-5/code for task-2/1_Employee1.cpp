/* a.Define the Employee structure with members: name (use an array of 100 characters), tax
number, and salary. Write a program that uses this type to read the data of 100 employees and
store them in an array of such structures. If the user enters fin for name, the data insertion
should terminate and the program should write the structures in the test.bin binary file.*/


#include <iostream>
#include <fstream>
using namespace std;

struct Employee
{
    string name;
    int tax_number;
    float salary;
};

int main()
{
    Employee employees[100]; // array of 100 employee instances
    int i;

    // Loop through 100 employees
    for (i = 0; i < 100; i++)
    {
        // To enter the name of the employee
        cout << "Enter the name of employee " << i + 1 << " (or 'fin' to exit): ";
        cin >> employees[i].name;

        // Check if the user entered 'fin'
        if (employees[i].name == "fin")
        {
            break;
        }
        // To enter the tax number of the employee
        cout << "Enter the tax number of employee " << i + 1 << ": ";
        cin >> employees[i].tax_number;

        // To enter the salary of the employee
        cout << "Enter the salary of employee " << i + 1 << ": ";
        cin >> employees[i].salary;
    }
    // Open a binary file for output operations
    ofstream file("test.bin", ios::binary);

    // Write data from memory to the file
    file.write((char *)employees, sizeof(Employee) * i);

    // Close the file
    file.close();

    return 0;
}