// FINAL PROJECT

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <vector>
#include "json.hpp"

using namespace std;

// this line is needed to use json
using json = nlohmann::json;

// structure client
struct client
{
    string name;
    int id;
    string address;
    string dob;
};

// structure stock
struct stock
{
    int stockId;
    string stockName;
    double marketPrice;
};

template <typename T>
class linkedList
{
    struct node
    {
        T data;
        node *next;
    };

    node *head = NULL;

public:
    void addNode(T newData)
    {
        node *newNode = new node;
        newNode->data = newData;
        newNode->next = head;
        head = newNode;
    }
};

class addData
{
protected:
    linkedList<stock> stockList;
    linkedList<client> clientList;
    json data;
    string fileName;

public:
    virtual void addNewData() = 0;
    void writeToFile()
    {
        ifstream i(fileName);
        json existingData = json::array();
        if (i.good())
        {
            i >> existingData;
            i.close();

            existingData.push_back(data);
        }
        else
        {
            existingData.push_back(data);
        }

        ofstream o(fileName);
        o << setw(4) << existingData << endl;
        o.close();

        // if it is a cleint, cout client added and if it is a stock, cout stock added
        if (fileName == "clients.json")
        {
            cout << "Client added Successfully." << endl;
        }
        else
        {
            cout << "Stock added Successfully." << endl;
        }
    }
};

class addClient : public addData
{
private:
    client newClient;
public:
    void addNewClient()
    {
        cout << "Enter client name: ";
        cin >> newClient.name;
        cout << "Enter client ID: ";
        cin >> newClient.id;
        cout << "Enter client address: ";
        cin >> newClient.address;
        cout << "Enter client date of birth: ";
        cin >> newClient.dob;

        // add client to linked list
        clientList.addNode(newClient);

        // add client data to json object
        data["name"] = newClient.name;
        data["id"] = newClient.id;
        data["address"] = newClient.address;
        data["dob"] = newClient.dob;

        fileName = "clients.json";
        writeToFile();
    }

    void addNewData()
    {
        addNewClient();
    }
};

class addStock : public addData
{
private:
    stock newStock;
public:
    void addNewStock()
    {
        cout << "Enter stock ID: ";
        cin >> newStock.stockId;
        cout << "Enter stock name: ";
        cin >> newStock.stockName;
        cout << "Enter stock market price: ";
        cin >> newStock.marketPrice;

        stockList.addNode(newStock);

        data["stockId"] = newStock.stockId;
        data["stockName"] = newStock.stockName;
        data["marketPrice"] = newStock.marketPrice;

        fileName = "stocks.json";
        writeToFile();
    }

    void addNewData()
    {
        addNewStock();
    }
};

class Transaction
{
private :
    int clientId;
    int stockId;
    int quantity;
    double totalPrice;
    double purchasedRate;
public:
    void purchaseStock()
    {
        json clients;
        // read clients.json file
        ifstream clientsFile("clients.json");
        if (clientsFile.good())
        {
            clientsFile >> clients;
            clientsFile.close();
        }
        else
        {
            cout << "Unable to open clients.json file" << endl;
            return;
        }

        // take user input
        cout << "Enter client ID: ";
        cin >> clientId;

        // check if client exists
        bool clientExists = false;
        for (int i = 0; i < clients.size(); i++)
        {
            if (clients[i]["id"] == clientId)
            {
                clientExists = true;
                break;
            }
        }

        if (!clientExists)
        {
            cout << "Client does not exist." << endl;
            return;
        }

        // display stocks available
        json stocks;
        ifstream stocksFile("stocks.json");
        if (stocksFile.good())
        {
            stocksFile >> stocks;
            stocksFile.close();
        }
        else
        {
            cout << "Unable to open stocks.json file" << endl;
            return;
        }

        // display stock and id to user
        cout << "Stocks available: " << endl;
        for (int i = 0; i < stocks.size(); i++)
        {
            cout << stocks[i]["stockId"] << ". " << stocks[i]["stockName"] << endl;
        }

        // extract market price of each stock
        vector<double> marketPrices;
        for (int i = 0; i < stocks.size(); i++)
        {
            marketPrices.push_back(stocks[i]["marketPrice"]);
        }

        // take user input
        cout << "Enter stock ID: ";
        cin >> stockId;

        // check if stock exists
        bool stockExists = false;
        for (int i = 0; i < stocks.size(); i++)
        {
            if (stocks[i]["stockId"] == stockId)
            {
                stockExists = true;
                break;
            }
        }

        if (!stockExists)
        {
            cout << "Stock does not exist." << endl;
            return;
        }

        // take user input
        cout << "Enter quantity: ";
        cin >> quantity;

        // use that extracted market price to calculate total price
        totalPrice = marketPrices[stockId - 1] * quantity;

        // display total price to user
        cout << "Total price: " << totalPrice << endl;

        // now we have all the data we need to add to the purchase.json file
        json purchaseData;
        purchaseData["clientId"] = clientId;
        purchaseData["stockId"] = stockId;
        purchaseData["quantity"] = quantity;
        purchaseData["totalPrice"] = totalPrice;
        // add market price to purchaseData
        purchaseData["purchasedRate"] = marketPrices[stockId - 1];

        // check if purchase.json file exists
        ifstream i("transaction.json");
        json existingData = json::array();
        if (i.good())
        {
            i >> existingData;
            i.close();

            // add new purchase data to existing data
            existingData.push_back(purchaseData);
        }
        else
        {
            existingData.push_back(purchaseData);
        }

        // write to purchase.json file
        ofstream o("transaction.json");
        o << setw(4) << existingData << endl;
        o.close();

        cout << "Purchase successful." << endl;
    }

    void sellStock()
    {
        cout << "Enter client ID: ";
        cin >> clientId;

        // check if client exists
        ifstream clientsFile("clients.json");
        json clients;
        bool clientExists = false;
        if (clientsFile.good())
        {
            clientsFile >> clients;
            clientsFile.close();

            for (int i = 0; i < clients.size(); i++)
            {
                if (clients[i]["id"] == clientId)
                {
                    clientExists = true;
                    break;
                }
            }
        }
        else
        {
            cout << "Unable to open clients.json file" << endl;
            return;
        }

        if (!clientExists)
        {
            cout << "Error: Client with ID " << clientId << " does not exist." << endl;
            return;
        }

        cout << "Enter stock ID: ";
        cin >> stockId;

        // check if stock exists
        ifstream stocksFile("stocks.json");
        json stocks;
        bool stockExists = false;
        if (stocksFile.good())
        {
            stocksFile >> stocks;
            stocksFile.close();

            for (int i = 0; i < stocks.size(); i++)
            {
                if (stocks[i]["stockId"] == stockId)
                {
                    stockExists = true;
                    break;
                }
            }
        }
        else
        {
            cout << "Unable to open stocks.json file" << endl;
            return;
        }

        if (!stockExists)
        {
            cout << "Error: Stock with ID " << stockId << " does not exist." << endl;
            return;
        }

        cout << "Enter quantity: ";
        cin >> quantity;

        // check if client has enough stock to sell

        ifstream i("transaction.json");
        json transactions;
        if (i.good())
        {
            i >> transactions;
            i.close();
        }
        else
        {
            cout << "Unable to open transaction.json file" << endl;
            return;
        }

        double totalPrice;
        for (json::iterator it = transactions.begin(); it != transactions.end(); ++it)
        {
            if (it->at("clientId") == clientId && it->at("stockId") == stockId)
            {
                int currentQuantity = it->at("quantity");
                if (quantity > currentQuantity)
                {
                    std::cout << "Error: Not enough stock to sell." << std::endl;
                    return;
                }

                purchasedRate = it->at("purchasedRate");
                totalPrice = purchasedRate * quantity;

                it->at("quantity") = currentQuantity - quantity;
                double price = it->at("totalPrice").get<double>();
                price = price - totalPrice;
                it->at("totalPrice") = price;

                std::cout << "Success: " << quantity << " stock sold for " << totalPrice << " dollars." << std::endl;
                break;
            }
        }

        ofstream o("transaction.json");
        o << setw(4) << transactions << endl;
        o.close();

        // extract the removed details and store in a new json file
        json soldStock;
        for (json::iterator it = transactions.begin(); it != transactions.end(); ++it)
        {
            if (it->at("clientId") == clientId && it->at("stockId") == stockId)
            {
                soldStock["clientId"] = it->at("clientId");
                soldStock["stockId"] = it->at("stockId");
                soldStock["quantity"] = quantity;
                soldStock["purchasedRate"] = it->at("purchasedRate");
                soldStock["totalPrice"] = totalPrice;
                break;
            }
        }

        // read the existing sold.json file
        ifstream is("sold.json");
        json existingSoldStock;
        if (is.good())
        {
            is >> existingSoldStock;
            is.close();
        }
        else
        {
            existingSoldStock = json::array();
        }

        // add the new sold stock to the existing sold.json file
        existingSoldStock.push_back(soldStock);

        ofstream os("sold.json");
        os << setw(4) << existingSoldStock << endl;
        os.close();
    }
};

class deleteCnS
{
    linkedList<int> clientIdList;
    linkedList<int> stockIdList;

public:
    void deleteClient()
    {
        int clientId;
        cout << "Enter client ID: ";
        cin >> clientId;
        clientIdList.addNode(clientId);

        ifstream i("clients.json");
        json clients;
        if (i.good())
        {
            i >> clients;
            i.close();
        }
        else
        {
            cout << "Unable to open clients.json file" << endl;
            return;
        }

        // delete client from clients.json
        json deletedClient;
        for (json::iterator it = clients.begin(); it != clients.end(); ++it)
        {
            if (it->at("id") == clientId)
            {
                deletedClient = *it;
                clients.erase(it);
                break;
            }
        }

        ofstream o("clients.json");
        o << setw(4) << clients << endl;
        o.close();

        cout << "Client with ID " << clientId << " deleted." << endl;

        // read the existing deletedclients.json file
        ifstream is("deletedclients.json");
        json existingDeletedClients = json::array();
        if (is.good())
        {
            is >> existingDeletedClients;
            is.close();
        }
        else
        {
            existingDeletedClients = json::array();
        }

        // add the deleted client to the existing deletedclients.json file
        if (!deletedClient.empty()){
            existingDeletedClients.push_back(deletedClient);
        }

        ofstream os("deletedclients.json");
        os << setw(4) << existingDeletedClients << endl;
        os.close();

        // if client is deleted, delete all the records of the client associated with the client
        ifstream itt("transaction.json");
        json transactions;
        if (itt.good())
        {
            itt >> transactions;
            itt.close();
        }
        else
        {
            cout << "Unable to open transaction.json file" << endl;
            return;
        }

        json deletedTransactions;
        for (json::iterator it = transactions.begin(); it != transactions.end(); ++it)
        {
            if (it->at("clientId") == clientId)
            {
                deletedTransactions.push_back(*it);
                transactions.erase(it);
            }
        }

        ofstream ot("transaction.json");
        ot << setw(4) << transactions << endl;
        ot.close();

        // remove the record from stocks.json
        ifstream ist("sold.json");
        json stocks;
        if (ist.good())
        {
            ist >> stocks;
            ist.close();
        }
        else
        {
            cout << "Unable to open stocks.json file" << endl;
            return;
        }

        json deletedStocks;
        for (json::iterator it = stocks.begin(); it != stocks.end(); ++it)
        {
            if (it->at("clientId") == clientId)
            {
                deletedStocks.push_back(*it);
                stocks.erase(it);
            }
        }

        ofstream ost("sold.json");
        ost << setw(4) << stocks << endl;
        ost.close();
    }

    void deleteStock()
    {
        int stockId;
        cout << "Enter stock ID to delete: ";
        cin >> stockId;
        stockIdList.addNode(stockId);

        ifstream i("stocks.json");
        json stocks;
        if (i.good())
        {
            i >> stocks;
            i.close();
        }
        else
        {
            cout << "Unable to open stocks.json file" << endl;
            return;
        }

        json deletedStock;
        for (json::iterator it = stocks.begin(); it != stocks.end(); ++it)
        {
            if (it->at("stockId") == stockId)
            {
                deletedStock = *it;
                stocks.erase(it);
                break;
            }
        }

        ofstream o("stocks.json");
        o << setw(4) << stocks << endl;
        o.close();

        cout << "Success: with stock ID " << stockId << " deleted" << endl;

        // read the existing deletedstocks.json file
        ifstream is("deletedStocks.json");
        json existingDeletedStocks = json::array();
        if (is.good())
        {
            is >> existingDeletedStocks;
            is.close();
        }
        else
        {
            existingDeletedStocks = json::array();
        }

        // add the deleted stock to the existing deletedstocks.json file
        if (!deletedStock.empty())
        {
            existingDeletedStocks.push_back(deletedStock);
        }

        ofstream os("deletedStocks.json");
        os << setw(4) << existingDeletedStocks << endl;
        os.close();

        // if stock is deleted, delete all the records of the stock associated with the stock
        ifstream itt("transaction.json");
        json transactions;
        if (itt.good())
        {
            itt >> transactions;
            itt.close();
        }
        else
        {
            cout << "Unable to open transaction.json file" << endl;
            return;
        }

        json deletedTransactions;
        for (json::iterator it = transactions.begin(); it != transactions.end(); ++it)
        {
            if (it->at("stockId") == stockId)
            {
                deletedTransactions.push_back(*it);
                transactions.erase(it);
            }
        }

        ofstream ot("transaction.json");
        ot << setw(4) << transactions << endl;
        ot.close();

        // remove the record from stocks.json
        ifstream ist("sold.json");
        json sold;
        if (ist.good())
        {
            ist >> sold;
            ist.close();
        }
        else
        {
            cout << "Unable to open stocks.json file" << endl;
            return;
        }

        json deletedSold;
        for (json::iterator it = sold.begin(); it != sold.end(); ++it)
        {
            if (it->at("stockId") == stockId)
            {
                deletedSold.push_back(*it);
                sold.erase(it);
            }
        }

        ofstream ost("sold.json");
        ost << setw(4) << sold << endl;
        ost.close();
    }

};

class Display
{
protected:
    json readJsonFile(string fileName)
    {
        ifstream i(fileName);
        json data;
        if (i.good())
        {
            i >> data;
            i.close();
        }
        else
        {
            cout << "Unable to open " << fileName << " file" << endl;
        }
        return data;
    }
};

class displayCnS : public Display
{
public:
    void displayClients()
    {
        // reads data from a json file called ("clients.json") 
        json clients = readJsonFile("clients.json");

        cout << "Clients:" << endl;
        /* The function then iterates over the json object and displays the data of each 
        client, including their ID, name, address and date of birth. */
        for (json::iterator it = clients.begin(); it != clients.end(); ++it)
        {
            cout << "ID: " << it->at("id") << endl;
            cout << "Name: " << it->at("name") << endl;
            cout << "Address: " << it->at("address") << endl;
            cout << "Date of Birth: " << it->at("dob") << endl;
        }
    }

    void displayStocks()
    {
        // reads data from a json file called ("stocks.json")
        json stocks = readJsonFile("stocks.json");

        cout << "Stocks:" << endl;
        for (json::iterator it = stocks.begin(); it != stocks.end(); ++it)
        {
            cout << "Stock ID: " << it->at("stockId") << endl;
            cout << "Stock Name: " << it->at("stockName") << endl;
            cout << "Stock Price: " << it->at("marketPrice") << endl;
            cout << endl;
        }
    }

    void displayPortfolio()
    {
        json transactions = readJsonFile("transaction.json");
        json clients = readJsonFile("clients.json");
        json stocks = readJsonFile("stocks.json");

        for (json::iterator it = transactions.begin(); it != transactions.end(); ++it)
        {
            int clientId = it->at("clientId");
            int stockId = it->at("stockId");
            int quantity = it->at("quantity");
            int totalPrice = it->at("totalPrice");

            string clientName;
            string stockName;
            for (json::iterator it = clients.begin(); it != clients.end(); ++it)
            {
                if (it->at("id") == clientId)
                {
                    clientName = it->at("name");
                    break;
                }
            }

            for (json::iterator it = stocks.begin(); it != stocks.end(); ++it)
            {
                if (it->at("stockId") == stockId)
                {
                    stockName = it->at("stockName");
                    break;
                }
            }

            cout << "Client Name: " << clientName << endl;
            cout << "Stock Name: " << stockName << endl;
            cout << "Quantity: " << quantity << endl;
            cout << "Total Price: " << totalPrice << endl;
            cout << endl;
        }
    }

    void displayDeletedClients()
    {
        ifstream i("deletedclients.json");
        json deleted_clients;
        if (i.good())
        {
            i >> deleted_clients;
            i.close();
        }
        else
        {
            cout << "Unable to open deleted_clients.json file" << endl;
            return;
        }

        cout << "Deleted Clients:" << endl;
        for (json::iterator it = deleted_clients.begin(); it != deleted_clients.end(); ++it)
        {
            cout << "ID: " << it->at("id") << endl;
            cout << "Name: " << it->at("name") << endl;
            cout << "Address: " << it->at("address") << endl;
            cout << "Date of Birth: " << it->at("dob") << endl;
            cout << endl;
        }
    }

    void displayDeletedStocks()
    {
        ifstream i("deletedstocks.json");
        json deleted_stocks;
        if (i.good())
        {
            i >> deleted_stocks;
            i.close();
        }
        else
        {
            cout << "Unable to open deleted_stocks.json file" << endl;
            return;
        }

        cout << "Deleted Stocks:" << endl;
        for (json::iterator it = deleted_stocks.begin(); it != deleted_stocks.end(); ++it)
        {
            cout << "Stock ID: " << it->at("stockId") << endl;
            cout << "Stock Name: " << it->at("stockName") << endl;
            cout << "Stock Price: " << it->at("marketPrice") << endl;
            cout << endl;
        }
    }
};

class mainNep
{
    addClient newClients;
    addStock newStocks;
    Transaction transaction;
    deleteCnS deleteCnS;
    displayCnS displayCnS;

public:
    mainNep()
    {
        cout << "Welcome to Stock Analyzer" << endl;
    }
    void mainMenu()
    {
        cout << "1. Add Client" << endl;
        cout << "2. Add Stock" << endl;
        cout << "3. Purchase Stock" << endl;
        cout << "4. Sell Stock" << endl;
        cout << "5. Delete" << endl;
        cout << "6. Display" << endl;
        cout << "7. Exit" << endl;
        cout << "Enter your choice:";
    }
    void choice()
    {
        while (true)
        {
            mainMenu();
            char choice;
            cin >> choice;

            switch (choice)
            {
            case '1':
                system("cls");
                cout << "------------------------" << endl;
                cout << "Add Client" << endl;
                cout << "------------------------" << endl;
                newClients.addNewClient();
                break;
            case '2':
                system("cls");
                cout << "------------------------" << endl;
                cout << "Add Stock" << endl;
                cout << "------------------------" << endl;
                newStocks.addNewStock();
                break;
            case '3':
                system("cls");
                cout << "------------------------" << endl;
                cout << "Purchase Stock" << endl;
                cout << "------------------------" << endl;
                transaction.purchaseStock();
                break;
            case '4':
                system("cls");
                cout << "------------------------" << endl;
                cout << "Sell Stock" << endl;
                cout << "------------------------" << endl;
                transaction.sellStock();
                break;
            case '5':
                system("cls");
                cout << "------------------------" << endl;
                cout << "1. Delete Client" << endl;
                cout << "2. Delete Stock" << endl;
                cout << "------------------------" << endl;
                cout << "Enter choice: ";
                cin >> choice;
                switch (choice)
                {
                case '1':
                    system("cls");
                    cout << "------------------------" << endl;
                    cout << "Delete Client" << endl;
                    cout << "------------------------" << endl;
                    deleteCnS.deleteClient();
                    break;
                case '2':
                    system("cls");
                    cout << "------------------------" << endl;
                    cout << "Delete Stock" << endl;
                    cout << "------------------------" << endl;
                    deleteCnS.deleteStock();
                    break;
                default:
                    cout << "Invalid choice." << endl;
                }
                break;
            case '6':
                system("cls");
                cout << "------------------------" << endl;
                cout << "1. Display Clients" << endl;
                cout << "2. Display Stocks" << endl;
                cout << "3. Display Portfolio" << endl;
                cout << "4. Display Deleted Clients" << endl;
                cout << "5. Display Deleted Stocks" << endl;
                cout << "------------------------" << endl;
                cout << "Enter choice: ";
                cin >> choice;
                switch (choice)
                {
                case '1':
                    system("cls");
                    cout << "------------------------" << endl;
                    cout << "Display Clients" << endl;
                    cout << "------------------------" << endl;
                    displayCnS.displayClients();
                    break;
                case '2':
                    system("cls");
                    cout << "------------------------" << endl;
                    cout << "Display Stocks" << endl;
                    cout << "------------------------" << endl;
                    displayCnS.displayStocks();
                    break;
                case '3':
                    system("cls");
                    cout << "------------------------" << endl;
                    cout << "Display Portfolio" << endl;
                    cout << "------------------------" << endl;
                    displayCnS.displayPortfolio();
                    break;
                case '4':
                    system("cls");
                    cout << "------------------------" << endl;
                    cout << "Display Deleted Clients" << endl;
                    cout << "------------------------" << endl;
                    displayCnS.displayDeletedClients();
                    break;
                case '5':
                    system("cls");
                    cout << "------------------------" << endl;
                    cout << "Display Deleted Stocks" << endl;
                    cout << "------------------------" << endl;
                    displayCnS.displayDeletedStocks();
                    break;
                default:
                    cout << "Invalid choice." << endl;
                }
                break;
            case '7':
                return;
            default:
                cout << "Invalid choice." << endl;
            }
        }
    }
};

int main()
{
    mainNep mainNepal; 
    mainNepal.choice();
    system("cls");

    return 0;
}
