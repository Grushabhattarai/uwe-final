/* 1. Write the declarations for a program that converts a weight in pounds
to a weight in kilograms.*/


#include <iostream>
using namespace std;

class weight
{
    float w;
    public:
        void getData();      // setter function decleration
        float calculation(); // getter function decleration
        void display();      //display function decleration
};
void weight::getData()
{
    cout<<"enter weight in pounds: "<<endl;
    cin>>w;
}
float weight::calculation()
{
    float converter;
    converter = w * 0.454592;
    return converter;
}
void weight::display()
{
    cout<<"weight in kg is: "<<calculation();
}
int main()
{
    weight w;
    w.getData();
    w.calculation();
    w.display();
    return 0;
}