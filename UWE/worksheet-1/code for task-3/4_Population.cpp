/* 4.There are 900 people in a town whose population increases by 10 percent (a data item)
each year. Write a loop that displays the annual population and determines how many years
(countYears) it will take for the population to pass 20,000.Verify that your program works
if the population doubles each year (an increase of 100%).*/

#include <iostream>
using namespace std;

class Town
{
    int startingPopulation;
    int annualIncrease;
    int endPopulation;
    int countYears;
    public:
    Town(int startingPopulation, int annualIncrease, int endPopulation)
    {
        this->startingPopulation = startingPopulation;
        this->annualIncrease = annualIncrease;
        this->endPopulation = endPopulation;
        countYears = 0;
    }

    void growth()
    {
        startingPopulation = startingPopulation + (startingPopulation * (annualIncrease / 100.0));
        countYears++;
    }

    bool isEnd()
    {
        if(startingPopulation >= endPopulation)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    void print()
    {
        cout << "Year " << countYears << ": " << startingPopulation << endl;
    }

};

// main function
int main(){
    Town t(900,10,20000);
    while(!t.isEnd())
    {
        t.growth();
    }
    t.print();
    return 0;
}