/*2. Write a program that reads two int values into m, n and displays their sum, their differences
 (m – n and n – m), their product, their quotients (m / n and n / m) and both m % n and n % m.
If the numbers are 4 and 5, the line that shows their sum should be displayed as: 5 + 4 = 9 */

#include <iostream>
using namespace std;

class Calculator
{
    int m,n;
    public:
        void getData(); //setter function
};
//setter function to set m
void Calculator::getData()
{
    cout <<" enter the value for m: ";
    cin >> m;
    cout<<" enter the value for n: ";
    cin >> n;
    cout << m << " + " << n << " = " << m + n << endl;
    cout << m << " - " << n << " = " << m - n << endl;
    cout << n << " - " << m << " = " << m - n << endl;
    cout << m << " * " << n << " = " << m * n << endl;
    cout << m << " / " << n << " = " << m / n << endl;
    cout << n << " / " << m << " = " << m / n << endl;
    cout << m << " % " << n << " = " << m % n << endl;
    cout << n << " % " << m << " = " << m % n << endl;
}
int main()
{
    // creating an object of class Calculator
    Calculator c;

    // set m and n
    c.getData();

    return 0;
}