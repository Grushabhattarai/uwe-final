/* 3. Implement the decision table below using a nested if statement. 
Assume that the grade point average is within the range 0.0 through 4.0.

Grade Point Average 		Transcript Message
0.0 to 0.99 			Failed semester—registration suspended
1.0 to 1.99 			On probation for next semester
2.0 to 2.99 			(no message)
3.0 to 3.49 			Dean’s list for semester
3.5 to 4.0 			Highest honors for semester */

#include <iostream>
using  namespace std;

class Marks
{
    float gpa;
    public:
        void getData();     // read values function declaration
        void displayData(); // getter function declaration
};

void Marks::getData()
{
    cout << "Enter you GPA: ";
    cin >> gpa;
}

void Marks::displayData()
{
    if(gpa >= 0.0 && gpa <= 0.99){
        cout << "Failed semester-registration suspended" << endl;
    }else if(gpa >= 1.0 && gpa <= 1.99){
        cout << "On probation for next semester" << endl;
    }else if(gpa >= 2.0 && gpa <= 2.99){
        cout << "(no message)" << endl;
    }else if(gpa >= 3.0 && gpa <= 3.49){
        cout << "dean's list for semester" << endl;
    }else if(gpa >= 3.5 && gpa <= 4.0){
        cout << "Highest honors for semester" << endl;
    }else{
        cout << "Invalid GPA" << endl;
    }
}
int main()
{
    Marks m;
    m.getData();
    m.displayData();
    return 0;
}