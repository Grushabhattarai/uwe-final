/* 2. Create a structure named Dog with a string field for the Dog’s name. Create a structure
named Cat with a string field for the Cat’s name. Write a program that declares one Dog and one
Cat, and assign names to them. Write two overloaded functions named speak().
If you pass the Dog to speak(), the speak()function should display the Dog’s name and
a description of how dogs speak (for example, “Spot says woof” ). If you pass the Cat to the 
version of speak()that accepts a Cat parameter, then it should display the Cat’s name and a
 description of how cats speak (for example, “Tiger says meow”). Save the file as CatDog.cpp.*/

 #include <iostream>
 #include <string>
 using namespace std;

//structure for a Dog
 struct Dog
 {
    string name;
 };

//structure for a Cat
 struct Cat
 {
    string name;
 };

//Overload function to display how dog speaks
 void speak(Dog dog)
 {
    cout << dog.name << "says woof" << endl;
 }

//Overload function to display how Cat speaks
 void speak (Cat cat)
 {
    cout << cat.name << "says meow" << endl;
 }

 int main()
 {
    Dog dog;
    dog.name = "Spot ";

    Cat cat;
    cat.name = "Tiger ";

    speak(dog);
    speak(cat);
    return 0;
 }

