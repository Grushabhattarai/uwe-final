/* 1.Write a program that uses the random number generator functions to generate a random 
sequence of letters from A to Z. The GenerateALetter function should return a letter based on
the ASCII table; that is, A = 65 and Z = 90. In main, above the “do again” loop, ask the user
to enter a seed value and call srand() passing it the seed value. Write out fifteen randomly-
generated characters on one line (separated by commas).

Note: You will need to place the call to GenerateALetter inside a  for loop and write one letter
at a time. Go through the following example to get an idea how to analyze a problem based on functions.

IsItPrime is Program. It asks the user to enter a positive integer value.
The program checks that the value is positive, and then determines if the number is prime. 
The program flow is illustrated in the figure below with the numbered arrows indicating the 
function call order and values that are passed between functions. This program calls the 
function AskPosNumber to get a value from the user. In the AskPosNumber function, it calls
the function CheckIt. This function checks to see if the value is positive (using a boolean
return value, true or false).If it is zero or negative, AskPosNumber asks the user to re-enter
the value. Once a positive integer is returned to main, we call the IsItPrime function, which
determines if the number is prime. The IsItPrime function returns a true or false to main.

The circled numbers indicate the order of the function calls and returned values.

From the diagram you will have some idea how functions are declared and called. Now write
a C++ programs for the following question.
*/

#include <iostream>
using namespace std;

char GenerateAletter()
{
    char randomNumber;
    randomNumber = rand() % 26 + 65; // it generates random num betn 0 to 25 and add 65 which is equal to A to Z in ASCII.
    return randomNumber;
}

int main()
{
    int seed;
    int i;

    cout<<"enter a seed value: ";
    cin >> seed;

    srand(seed);

    for(i=0; i < 15; ++i)
    {
        cout<< GenerateAletter() << ", ";
    }
    return 0;
}