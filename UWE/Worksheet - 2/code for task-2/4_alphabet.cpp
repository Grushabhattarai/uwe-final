/* 4. Specification: Fill a character array with the uppercase alphabet (place A in
[0], B in [1], etc.).
 */

#include <iostream>
using namespace std;

int main()
{
    char alphabet[26];
    int i;
    for (i=0; i < 26; ++i)
    {
        alphabet[i] = 'A' + i;
    }

    for (i=0; i < 26; ++i)
    {
        cout << alphabet[i] <<endl;
    }
    return 0;
}