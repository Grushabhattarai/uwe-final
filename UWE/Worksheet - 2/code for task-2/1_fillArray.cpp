/* 1. */

#include <iostream>
using namespace std;

void FillArray(char values[], int size)
{
    for (int i = 0; i < size; i++)
    {
        values[i]='a';
    }
}

int main()
{
    int size = 75;
    char values[size];
    FillArray(values,size);
    return 0;
}