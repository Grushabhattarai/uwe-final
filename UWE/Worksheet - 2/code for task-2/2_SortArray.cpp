/* 2. */

#include <iostream>
using namespace std;

void sortArray(int values[]);

int main()
{
    int values[75];

    //assume values becomes filled with data
    sortArray(values);
    return 0;
}

void sortArray(int values[])
{
    for(int i = 0; i < 75; ++i)
    {
        if(values[i] < values[i - 1])
        {
            values[i] = values[i - 1];
        }
    }
}