/* 3. Specification: Ask the user for a character array and then reverse the characters.
If the user entered Hello World, the new string would read dlroW olleH.
*/

#include <iostream>
#include <cstring>
using namespace std;

int main()
{
    char saying[50],revSaying[50];
    int i;

    cout<<"\nEnter a saying: ";
    cin.getline(saying, sizeof(saying) / sizeof(char));

    for (i = 0; i<strlen(saying); i++)
    {
        revSaying[i]=saying[strlen(saying)-1-i];
    }
    cout<<"reversed saying: "<<revSaying<<endl;
    return 0;
}