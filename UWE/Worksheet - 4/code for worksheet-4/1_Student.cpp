#include <iostream>
using namespace std;

class Student
{
private:
  int code;      // Student's identifier
  int courses;   // Number of courses
  float *grades; // Grades in all courses

public:
  // constructor with default values
  Student(int code = 0, int courses = 0, int *grades = 0)
  {
    this->code = code;
    this->courses = courses;
    this->grades = new float[courses]; // Dynamic memory allocation
  }

  // Copy constructor (deleted)
  Student(Student &other) = delete;

  // Assignment operator (deleted)
  Student &operator=(Student &other) = delete;

  // Overloaded input operator
  friend istream &operator>>(istream &is, Student &s)
  {
    // Read code and number of courses
    is >> s.code >> s.courses;

    // deallocate the memory allocated for the array of grades
    delete[] s.grades;

    // Allocate memory for the new grades array
    s.grades = new float[s.courses];

    // Read the grades
    for (int i = 0; i < s.courses; ++i)
    {
      is >> s.grades[i];
    }

    return is;
  }

  // Overloaded comparison operator
  friend int operator>(Student &s1, Student &s2)
  {
    int passedCourses1 = 0, passedCourses2 = 0;
    for (int i = 0; i < s1.courses; ++i)
    {
      if (s1.grades[i] >= 5)
      {
        passedCourses1++;
      }
    }

    for (int i = 0; i < s2.courses; ++i)
    {
      if (s2.grades[i] >= 5)
      {
        passedCourses2++;
      }
    }

    if (passedCourses1 > passedCourses2)
    {
      return 1;
    }
    else if (passedCourses1 < passedCourses2)
    {
      return 2;
    }
    else
    {
      return 3;
    }
  }

  // Return the code of the student
  int getCode() 
  { 
    return code; 
  }
};

int main()
{
  Student s1, s2;
  cin >> s1 >> s2;

  // Compare the number of passed courses
  int result = (s1 > s2);

  /* Check the result and display the code of the student who has succeeded in most courses 
  or Same if they have succeeded in the same number */
  if (result == 1)
  {
    cout << "First student has succeeded in more courses" << endl;
    cout << "Code: " << s1.getCode() << endl; 
  }
  else if (result == 2)
  {
    cout << "Second student has succeeded in more courses" << endl;
    cout << "Code: " << s2.getCode() << endl;
  }
  else
  {
    cout << "Same" << endl;
  }

  return 0;
}