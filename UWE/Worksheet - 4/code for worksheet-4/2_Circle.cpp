#include <iostream>
using namespace std;

class Circle
{
private:
    float rad; // The radius of the circle.
public:
    Circle(float r) : rad(r) {}          // Constructor for the Circle class.
    float getRadius() { return rad; }    // Get the radius of the circle.
    void setRadius(float r) { rad = r; } // Set the radius of the circle.

    Circle &operator-(int);
};

Circle &Circle::operator-(int n)
{
    float rad = getRadius() - n; // subtract n from radius
    if (rad < 0)
    {
        throw 10; // throw 10 if radius is negative
    }
    setRadius(rad); // set radius to new value
    return *this;   // return circle object
}

class Ellipse : public Circle
{
private:
    float axis; // The semi-major axis of the ellipse.
public:
    Ellipse(float r, float a) : Circle(r), axis(a) {} // Constructor for the Ellipse class.
    // Overload the - operator to subtract the given value from the radius and axis of the ellipse.
    float getSemiMajorAxis() { return axis; }    // get semi-major axis of ellipse
    void setSemiMajorAxis(float s) { axis = s; } // set semi-major axis of ellipse
    Ellipse &operator-(int);
};

Ellipse &Ellipse::operator-(int n)
{
    axis = axis - n; // subtract n from semi-major axis
    if (axis < 0)
    {
        throw 20; // throw 20 if semi-major axis is negative
    }
    setSemiMajorAxis(axis); // set semi-major axis to new value

    float rad = getRadius() - n; // subtract n from radius
    if (rad < 0)
    {
        throw 10; // throw 10 if radius is negative
    }
    setRadius(rad); // set radius to new value

    return *this; // return ellipse object
}

void f(Circle &r, int n)
{
    try
    {
        r - n;
        cout << "Radius of the circle: " << r.getRadius() << endl;
    }
    catch (int e)
    {
        if (e == 10)
        {
            cout << "Radius of the circle cannot be negative." << endl;
        }
    }
}

void f(Ellipse &r, int n)
{
    try
    {
        r - n;
        cout << "Radius of the semi-circle: " << r.getRadius() << endl;
        cout << "Semi-major axis of the ellipse: " << r.getSemiMajorAxis() << endl;
    }
    catch (int e)
    {
        if (e == 10)
        {
            cout << "Radius of the circle cannot be negative." << endl;
        }
        else if (e == 20)
        {
            cout << "Semi-major axis of the ellipse cannot be negative." << endl;
        }
    }
}

int main()
{
    Circle cir(5);      // The radius of the circle should become 5.
    Ellipse ell(10, 6); /* The radius of the circle should become 10 and the axis of the ellipse 6.
    The constructor of the Ellipse should call the constructor of the Circle. */
    Circle &r1 = cir;
    Ellipse &r2 = ell;
    // Pass the Ellipse object directly to the f() function.
    f(r1, 3);
    f(r2, 1);
    f(r2, 10);
    return 0;
}