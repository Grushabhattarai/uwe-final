#include <iostream>
using namespace std;

class School
{
private:
    string name;

public:
    // constructor to set the name of the school
    School(const string& n) : name(n) {}

    // default constructor
    School() {}

    // virtual destructor
    virtual ~School() {}

    // virtual method to show the name of the school
    virtual void show() const { cout << "Name of school: " << name << endl; }
};

class Programming : virtual public School
{
private:
    int numCourses;

public:
    // constructor to set the name of the school and number of programming courses
    Programming(const string& n, int num) : School(n), numCourses(num) {}

    // To show the number of programming courses
    void show() const 
    {
        cout << "Programming courses: " << numCourses << endl;
    }
};

class Network : virtual public School
{
private:
    int numCourses;

public:
    // constructor to set the name of the school and number of network courses
    Network(const string& n, int num) : School(n), numCourses(num) {}

    // To show the number of network courses
    void show() const
    {
        cout << "Network courses: " << numCourses << endl;
    }
};

class Student : public Programming, public Network
{
private:
    string name;
    int code;

public:
    // constructor to set the name of the school, name and code of the student, and the number of programming and network courses
    Student(const string& schoolName, const string& studentName, int studentCode, int numProgCourses, int numNetCourses)
        : School(schoolName), Programming(schoolName, numProgCourses), Network(schoolName, numNetCourses), name(studentName), code(studentCode) {}

    // To show the details of the student
    void show() const 
    {
        // calling the show method of the base class
        School::show();
        Programming::show();
        Network::show();
        cout << "Student name: " << name << endl;
        cout << "Student code: " << code << endl;
    }
};

int main()
{
    Student s("TBC", "Prabhat", 100, 10, 12);
    School& p = s;
    p.show();
    return 0;
}
