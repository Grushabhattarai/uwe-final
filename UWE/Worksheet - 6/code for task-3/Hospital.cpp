
#include <iostream>
#include <queue>
using namespace std;

class Patient
{
public:
    string patientid;
    int social_id;
    char occupation;
    char disease;
    string treatment;
    bool prev_treated;
    int token_number;
    string dateofVisit;

    // default constructor
    Patient() {}

    // Parametrized constructor to initialize the member variables
    Patient(string patientid, int social_id, char occupation, char disease, string treatment, bool prev_treated, int token_number, string dateofVisit)
    {
        this->patientid = patientid;
        this->social_id = social_id;
        this->occupation = occupation;
        this->disease = disease;
        this->treatment = treatment;
        this->prev_treated = prev_treated;
        this->token_number = token_number;
        this->dateofVisit = dateofVisit;
    }
};

class Hospital
{
private:
    int next_token_number;

public:
    queue<Patient> dermatology_queue;   // queue for dermatology department
    queue<Patient> cardiology_queue;    // queue for cardiology department
    queue<Patient> gynecology_queue;    // queue for gynecology department
    queue<Patient> pathology_queue;     // queue for pathology department


    queue<Patient> treated_patients;     // queue for treated patients

    
    // default constructor
    Hospital()
    {
        next_token_number = 1;
    }

    // function to get the size of dermatology queue
    int getDermatologyQueueSize()
    {
        return dermatology_queue.size();
    }

    // function to get the size of cardiology queue
    int getCardiologyQueueSize()
    {
        return cardiology_queue.size();
    }

    // function to get the size of gynecology queue
    int getGynecologyQueueSize()
    {
        return gynecology_queue.size();
    }

    // function to get the size of pathology queue
    int getPathologyQueueSize()
    {
        return pathology_queue.size();
    }

    // function to get the size of treated patients queue
    int getTreatedPatientsSize()
    {
        return treated_patients.size();
    }

    // function to add a patient to the appropriate department queue
    void addPatient(string patientid, int social_id, char occupation, char disease, string treatment, bool prev_treated, string dateofVisit)
    {
        Patient p(patientid, social_id, occupation, disease, treatment, prev_treated, next_token_number, dateofVisit);
        switch (disease)
        {
        case 'D':
            dermatology_queue.push(p);
            break;
        case 'C':
            cardiology_queue.push(p);
            break;
        case 'G':
            gynecology_queue.push(p);
            break;
        case 'P':
            pathology_queue.push(p);
            break;
        }
        next_token_number++;
    }

    // function to move a patient from department queue to treated patients queue
    void moveToTreated(char disease)
    {
        Patient p;
        switch (disease)
        {
        case 'D':
            p = dermatology_queue.front();
            dermatology_queue.pop();
            break;
        case 'C':
            p = cardiology_queue.front();
            cardiology_queue.pop();
            break;
        case 'G':
            p = gynecology_queue.front();
            gynecology_queue.pop();
            break;
        case 'P':
            p = pathology_queue.front();
            pathology_queue.pop();
            break;
        }
        cout << "Patient with token number: " << p.token_number << " is treated." << endl;
        treated_patients.push(p);
    }

    // function to count the number of patients per day
    int countPatientsPerDay(Hospital h, string date)
    {
        int count = 0;
        queue<Patient> all_queues[4] = {h.dermatology_queue, h.cardiology_queue, h.gynecology_queue, h.pathology_queue};
 
        for (int i = 0; i < 4; i++)
        {
            while (!all_queues[i].empty())
            {
                Patient p = all_queues[i].front();
                if (p.dateofVisit == date)
                {
                    count++;
                }
                all_queues[i].pop();
            }
        }
        return count;
    }
};

// function to count total patients in a particular department queue
int totalPatientsInQueue(Hospital h, char disease)
{
    switch (disease)
    {
    case 'D':
        return h.getDermatologyQueueSize();
    case 'C':
        return h.getCardiologyQueueSize();
    case 'G':
        return h.getGynecologyQueueSize();
    case 'P':
        return h.getPathologyQueueSize();
    default:
        return 0;
    }
}

// function to count total patients in the hospital
int totalPatientsInHospital(Hospital h)
{
    return h.getDermatologyQueueSize() + h.getCardiologyQueueSize() + h.getGynecologyQueueSize() + h.getPathologyQueueSize();
}

int main()
{
    Hospital h;
    h.addPatient("John Doe", 1, 'M', 'C', "Heart surgery", false, "01/01/2023");
    h.addPatient("grusha", 11, 'M', 'C', "Heart surgery", false, "01/01/2023");

    h.addPatient("Jane Doe", 2, 'F', 'D', "Acne treatment", true, "01/02/2023");
    h.addPatient("Bob Smith", 3, 'M', 'G', "Pregnancy check-up", false, "01/03/2023");
    h.addPatient("Samantha James", 4, 'F', 'P', "Blood test", true, "01/04/2023");
    h.addPatient("james smith", 4, 'F', 'P', "Blood test", true, "01/04/2023");

    cout << "------------------------------------------" << endl;
    cout << "Total patients on 01/01/2023: " << h.countPatientsPerDay(h, "01/01/2023") << endl;
    cout << "------------------------------------------" << endl;

    cout << endl;

    cout << "Total patients in cardiology queue: " << totalPatientsInQueue(h, 'C') << endl;
    cout << "Total patients in dermatology queue: " << totalPatientsInQueue(h, 'D') << endl;
    cout << "Total patients in gynecology queue: " << totalPatientsInQueue(h, 'G') << endl;
    cout << "Total patients in pathology queue: " << totalPatientsInQueue(h, 'P') << endl;
    cout << endl;
    cout << "Total patients in the hospital: " << totalPatientsInHospital(h) << endl;

    cout << endl;
    cout << "------------------------------------------" << endl;
    cout << endl;

    // Move a patient to the treated patients queue
    h.moveToTreated('C'); // Move a patient from cardiology queue to treated patients queue

    cout << "Total patients in treated patients queue: " << h.getTreatedPatientsSize() << endl;
    cout << "Total patients in cardiology queue: " << totalPatientsInQueue(h, 'C') << endl;
    cout << "Total patients in the hospital: " << totalPatientsInHospital(h) << endl;
 
    return 0; 
} 