/*  (b) To convert a decimal to binary form using stack */

#include <iostream>
#include <stack>
using namespace std;

class DecimalToBinaryConverter 
{
    private:
        int decimalNumber;
        stack<int> binaryStack;

    public:
        DecimalToBinaryConverter(int decimal) 
        {
            decimalNumber = decimal;
        }
        void convert() 
        {
            while (decimalNumber > 0) 
            {
                binaryStack.push(decimalNumber % 2);
                decimalNumber /= 2;
            }
        }
        void printBinary() 
        {
            while (!binaryStack.empty()) 
            {
                cout << binaryStack.top();
                binaryStack.pop();
            }
        }
};

int main() 
{
    DecimalToBinaryConverter converter(10);
    converter.convert();
    converter.printBinary();
    return 0;
}