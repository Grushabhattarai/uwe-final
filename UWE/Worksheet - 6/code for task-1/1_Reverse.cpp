/* Write the code for using STL containers and STL algorithms for the following:

 (a) To reverse a list
 
*/

#include <iostream>
#include <list>
#include <algorithm>
using namespace std;

//Class MyList
class MyList 
{
    private:
        //list data structure
        list<int>myList;

    public:
        //Constructor that initializes the list
        MyList() 
        {
            myList = {1, 2, 3, 4, 5};
        }
        //reverse the order of elements in the list
        void reverseList() 
        {
            reverse(myList.begin(), myList.end());
        }
        //print elements of the list
        void printList() 
        {
            for (auto i : myList) 
            {
                cout << i << " ";
            }
        }
};

int main() 
{
    //Create object of MyList class
    MyList myListObject;
    //Reverse the list
    myListObject.reverseList();
    //Print the list
    myListObject.printList();
    return 0;
}
