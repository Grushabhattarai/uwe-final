/* (c) Queue operations  */

#include<iostream>
#include <queue>
using namespace std;

class MyQueue
{
private:
    queue<int> myQueue;

public:
    void enqueue(int data)
    {
        myQueue.push(data);
    }
    int dequeue()
    {
        int data = myQueue.front();
        myQueue.pop();
        return data;
    }
};

int main()
{
    MyQueue myQueueObject;
    myQueueObject.enqueue(1);
    myQueueObject.enqueue(2);
    myQueueObject.enqueue(3);

    cout << myQueueObject.dequeue() << endl;
    cout << myQueueObject.dequeue() << endl;
    cout << myQueueObject.dequeue() << endl;
    return 0;
}