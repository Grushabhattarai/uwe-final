/* 1. Create a class named Instructor. It contains a first and last name and an office number,
and its only constructor requires all three as arguments. Create a class named Classroom. It
contains a building and a room number, and its only constructor requires both as arguments.
Create a class named CollegeCourse. A CollegeCoursecontains an Instructor, a Classroom, and a
number of credits. Its constructor requires a first and last name of an instructor, the
instructor’s office number, a Classroombuilding and room number, and a number of credits. Each
of these classes contains a function that displays an object’s values. Write a main()function
that instantiates at least two CollegeCourseobjects and displays their values. Save the file
as CollegeCourse.cpp */

#include <iostream>
using namespace std;

class Instructor
{
private:
    string firstName;
    string lastName;
    int officeNo;

public:
    // constructor to initialize the member variables
    Instructor(string firstName, string lastName, int officeNo)
    {
        this->firstName = firstName;
        this->lastName = lastName;
        this->officeNo = officeNo;
    }

    // method to display the details of the instructor
    void display()
    {
        cout << "Instructor: " << firstName << " " << lastName << endl;
        cout << "officeNo: " << officeNo << endl;
    }
};

class Classroom
{
private:
    string building;
    int roomNumber;

public:
    // constructor to initialize the member variables
    Classroom(string building, int roomNumber)
    {
        this->building = building;
        this->roomNumber = roomNumber;
    }

    // method to display the details of the classroom
    void display()
    {
        cout << "Classroom: " << building << " " << roomNumber << endl;
    }
};

class CollegeCourse
{
private:
    Instructor instructor;
    Classroom classroom;
    int credits;

public:
    // constructor to initialize the member variables
    CollegeCourse(string firstName, string lastName, int officeNo, string building, int roomNumber, int credits)
                : instructor(firstName, lastName, officeNo), classroom(building, roomNumber)
    {
        this->credits = credits;
    }

    // method to display the details of the college course
    void display()
    {
        instructor.display();
        classroom.display();
        cout << "Credits: " << credits << endl;
    }
};
int main()
{
    CollegeCourse course1("Shahid", "Kapoor", 50, "S", 100, 5);
    CollegeCourse course2("Disha", "Patani", 200, "D", 400, 6);
    course1.display();
    course2.display();
    return 0;
}