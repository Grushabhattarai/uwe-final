/* 2. a. Create two classes. The first, named Sale, holds data for a sales transaction.  Its 
private data members include the day of the month, amount of the sale, and the salesperson’s ID
number. The second class, named Salesperson, holds data for a salesperson, and its private data
members include each salesperson’s ID number and last name. Each class includes a constructor to
which you can pass the field values. Create a friend function named display()that is a friend of
both classes and displays the date of sale, the amount, and the salesperson ID and name. Write a
short main()demonstration program to test your classes and friendfunction. Save the file as 
Sales.cpp. */

#include <iostream>
#include <string>
using namespace std;

// class decleration
class Salesperson;

class Sale
{
    private:
    int day;         // variable to store day of the sale
    double amount;   // variable to store the amount of sale
    int id;         // variable to store the id of sale

    public:
    Sale(int day, double amount, int id)
    {
        this -> day = day;
        this -> amount = amount;
        this -> id = id;
    }

    /* Declaring display function as friend of the class so that it can access the private 
    members of the class */
    friend void display(Sale sale, Salesperson salesperson);
};

class Salesperson
{
    private:
    int id;       // variable to store the id of the salesperson
    string name;  // variable to store the name of the salesperson
    
    public:
    Salesperson(int id, string name)
    {
        this -> id = id;
        this -> name = name;
    }
    friend void display(Sale sale, Salesperson salesperson);
};

// display function to show the details of the sale and salesperson
void display(Sale sale, Salesperson salesperson)
{
    cout << "Day: " << sale.day << endl;
    cout << "Amount: " << sale.amount << endl;
    cout << "Salesperson ID: " << salesperson.id << endl;
    cout << "Salesperson name: " << salesperson.name << endl;
}

int main()
{
    Sale sale(2, 200, 1);
    
    Salesperson salesperson(1, "grusha");

    //Calling the display function to display the details of the sale and salesperson
    display(sale, salesperson);
    return 0;
}