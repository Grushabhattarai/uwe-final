/* 3. Add a function to both the Saleand Salespersonclasses that returns the private salesperson
ID number. Write a main()function that contains an array of five Salespersonobjects and store 
appropriate data in it. Then, continue to prompt the user for Saledata until the user enters an
appropriate sentinel value. For each Sale transaction entered, determine whether the salesperson’s
ID number is valid. Either display an error message, or use the frienddisplay()function to display
all the data. Save the file as Sales2.cpp. */

#include <iostream>
#include <string>
using namespace std;

// Salesperson class declaration
class Salesperson;

class Sale
{
private: 
    int day;         // variable to store the day of the sale
    double amount;   // variable to store the amount of the sale
    int id;          // variable to store the salesperson id of the sale

public:
    Sale(int day, double amount, int id)
    {
        this->day = day;
        this->amount = amount;
        this->id = id;
    }
    friend void display(Sale sale, Salesperson salesperson);

    
    // getter function to return the salesperson id of the sale
    int getSalespersonID()
    {
        return id;
    }
};

class Salesperson
{
private:
    int id;        // variable to store the id of the salesperson
    string name;   // variable to store the name of the salesperson

public:
    Salesperson(int id, string name)
    {
        this->id = id;
        this->name = name;
    }
    friend void display(Sale sale, Salesperson salesperson);

    // getter function to return the id of the salesperson
    int getSalespersonID()
    {
        return id;
    }
};

// display function to show the details of the sale and salesperson
void display(Sale sale, Salesperson salesperson)
{
    cout << "Day: " << sale.day << endl;
    cout << "Amount: " << sale.amount << endl;
    cout << "Salesperson ID: " << salesperson.id << endl;
    cout << "Salesperson name: " << salesperson.name << endl;
}

int main()
{
    // Array of Salesperson objects
    Salesperson salespersons[5] = {
        Salesperson(1, "bhattarai"),
        Salesperson(2, "ghimire"),
        Salesperson(3, "thapa"),
        Salesperson(4, "karki"),
        Salesperson(5, "magar")};
    
    // Sentinel value for the sales data input loop
    const int SENTINEL = -1;

    int day;
    double amount;
    int id;

    cout << "Enter the sales data (-1 to stop): ";
    cin >> day;

    // Loop to take input for the sales data
    while (day != SENTINEL)
    {
        cout << "Enter the amount of the sale: ";
        cin >> amount;
        cout << "Enter the salesperson ID: ";
        cin >> id;
        
        // Check if the salesperson ID is valid
        bool found = false;
        for (int i = 0; i < 5; i++)
        {
            //checking if the id of the salesperson from the array is same as the id entered
            if (salespersons[i].getSalespersonID() == id)
            {
                found = true;
                break;
            }
        }
        if (found){
            Sale sale(day, amount, id);
            for (int i = 0; i < 5; i++)
            {
                if (salespersons[i].getSalespersonID() == id)
                {
                    display(sale, salespersons[i]);
                    break;
                }
            }
        }
        else
        {
            cout << "Invalid salesperson ID" << endl;
        }
        cout << "Enter the sales data (-1 to stop): " << endl;
        cin >> day;
    }
    return 0; 
}